//
//  APIRouter.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Alamofire

private struct Config {
    #if DEBUG
    static let baseURL = "https://twg.azure-api.net/bolt"
    #else
    static let baseURL = "https://twg.azure-api.net/bolt"
    #endif
    
    static let SubscriptionKey = "8d630b83be484023ad4f80e651f31354"
}

enum ContentType: String {
    case json = "application/json"
}

enum APIRouter: URLRequestConvertible {
    case newUser
    case search(term: String, start: Int, limit: Int, userId: String)
    case price(barcode: String, userId: String)
    
    // MARK: - HTTPMethod
    private var method: HTTPMethod {
        return .get
    }
    
    // MARK: - Path
    private var path: String {
        switch self {
        case .newUser:
            return "/newuser.json"
        case .search:
            return "/search.json"
        case .price:
            return "/price.json"
        }
    }
    
    // MARK: - Parameters
    private var parameters: Parameters? {
        switch self {
        case .newUser:
            return [:]
        case .search(let term, let start, let limit, let userId):
            return [
                "Branch": 208,
                "Search": term,
                "UserID": userId,
                "Start": start,
                "Limit": limit
            ]
        case .price(let barcode, let userId):
            return [
                "Branch": 208,
                "Barcode": barcode,
                "UserID": userId
            ]
        }
    }
    
    // MARK: - URLRequestConvertible
    func asURLRequest() throws -> URLRequest {
        let url = try Config.baseURL.asURL()
        
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Common Headers
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: "Accept")
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField: "Content-Type")
        urlRequest.setValue(Config.SubscriptionKey, forHTTPHeaderField: "Ocp-Apim-Subscription-Key") // Each calls must contain subscription key
        
        // Parameters
        if let parameters = parameters {
            do {
                urlRequest = try URLEncoding.default.encode(urlRequest, with: parameters)
            } catch {
                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            }
        }
        
        return urlRequest
    }
}
