//
//  APIClient.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Alamofire

typealias UserIDCompletion = (String?) -> Void
typealias NewUserCompletion = (Result<UserResponse, Error>) -> Void
typealias SearchCompletion = (Result<SearchResponse, Error>) -> Void
typealias PriceCompletion = (Result<PriceResponse, Error>) -> Void

@objcMembers
class APIClient: NSObject {
    static var _jsonDecoder: JSONDecoder?
    static var jsonDecoder: JSONDecoder {
        if _jsonDecoder == nil {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .custom({ (keys) -> CodingKey in
                let key = keys.last!.stringValue
                // Convert 1st character to lowercase in keys of response json
                let deCapitalizedKey = key.prefix(1).lowercased() + key.dropFirst()
                return AnyKey(stringValue: deCapitalizedKey)!
            })
            
            _jsonDecoder = decoder
        }
        
        return _jsonDecoder!
    }
    
    static func getNewUser(completion:@escaping NewUserCompletion) {
        AF.request(APIRouter.newUser)
            .responseDecodable(decoder: jsonDecoder) { (response: DataResponse<UserResponse>) in
                completion(response.result)
        }
    }
    
    // Limit results of every search call
    static let searchLimit = 30
    
    static func search(withTerm term: String, start: Int = 1, limit: Int = searchLimit, completion:@escaping SearchCompletion) {
        getUserId { (userId) in
            guard userId != nil && !(userId!.isEmpty) else {
                return
            }
            AF.request(APIRouter.search(term: term, start: start, limit: limit, userId: userId!))
                .responseDecodable(decoder: jsonDecoder) { (response: DataResponse<SearchResponse>) in
                    completion(response.result)
            }
        }
    }
    
    static func price(withBarcode barcode: String, completion:@escaping PriceCompletion) {
        getUserId { (userId) in
            guard userId != nil && !(userId!.isEmpty) else {
                return
            }
            AF.request(APIRouter.price(barcode: barcode, userId: userId!))
                .responseDecodable(decoder: jsonDecoder) { (response: DataResponse<PriceResponse>) in
                    completion(response.result)
            }
        }
    }
}

// MARK: - User ID
extension APIClient {
    static let userIdStoreKey = "USERID"
    
    static func getUserId(completion:@escaping UserIDCompletion) {
        let userId = UserDefaults.standard.string(forKey: userIdStoreKey)
        
        if userId == nil || userId!.isEmpty {
            getNewUser {
                switch $0 {
                case .success(let userResponse):
                    let newUserId = userResponse.userID
                    UserDefaults.standard.set(newUserId, forKey: userIdStoreKey)
                    completion(newUserId)
                case .failure(let error):
                    print(error)
                    completion(nil)
                }
            }
        } else {
            completion(userId)
        }
    }
}
