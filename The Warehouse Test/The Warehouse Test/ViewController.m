//
//  ViewController.m
//  The Warehouse Test
//
//  Created by John Douglas on 10/10/18.
//  Copyright © 2018 The Warehouse Group Limited. All rights reserved.
//

#import "ViewController.h"
#import "NSString+Utils.h"

@interface ViewController ()

-(void)addNewString:(NSString *)👹;
-(void)addString:(NSString *)👺;
-(NSString *)getString;
-(void)buildString;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildString];
}

- (void)buildString {
    [self addNewString:@" "];
    [self addString:@" "];
    for (int i = 0; i < (int)22.0/2; i++) {
        if (i == (int)22.0/2 - 1) {
            [self addNewString:@"😬"];
            [self addString:@"😬"];
        } else {
            if (i % 3 == 0) {
                [self addNewString:@"📸"];
            } else if (i % 3 == 1) {
                [self addString:@"📷"];
            } else {
                [self addNewString:@"💯 "];
                [self addString:@" 💯"];
            }
        }
    }
}

- (void)addString:(NSString *)👹 {
    NSString *c = self.sBB.currentTitle;
    NSString *u = [c stringByAppendingString:👹];
    NSString *v = [c stringByAppendingString:👹];
    [self.sBB setTitle:v forState:UIControlStateNormal];
}

- (void)addNewString:(NSString *)👺 {
    NSString *c = self.sBB.currentTitle;
    NSString *🤕 = [c mutateWithString:👺];
    [self.sBB setTitle:🤕 forState:UIControlStateNormal];
}

- (NSString *)getString {
    NSString *c = self.sBB.currentTitle;
    return c;
}

@end
