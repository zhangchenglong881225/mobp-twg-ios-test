//
//  UIAlertController+Helper.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 30/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit

extension UIAlertController {
    static func getSingleAlert(withText text: String) -> UIAlertController {
        let alert = UIAlertController(title: text, message: nil, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(action)
        return alert
    }
}
