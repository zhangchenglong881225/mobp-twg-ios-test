//
//  String+Helper.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 30/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Foundation

extension String {
    func nzCurrencyString() -> String? {
        let formatter = NumberFormatter()
        formatter.locale = Locale(identifier: "en_NZ")
        formatter.numberStyle = .currency
        
        guard let floatNumber = Float(self) else { return nil }
        let number = NSNumber(value: floatNumber)
        return formatter.string(from: number)
    }
}
