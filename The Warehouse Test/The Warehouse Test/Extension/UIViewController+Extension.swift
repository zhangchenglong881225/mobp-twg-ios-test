//
//  UIViewController+Extension.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 30/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

extension UIViewController {
    func bindErrorAlert(viewModel: ErrorShowable, disposeBag: DisposeBag) {
        viewModel.onShowError
            .bind { [weak self] (errorMessage) in
                guard !errorMessage.isEmpty else {
                    return
                }
                self?.present(UIAlertController.getSingleAlert(withText: errorMessage), animated: true, completion: nil)
            }
            .disposed(by: disposeBag)
    }
    
    func bindLoadingIndicator(viewModel: LoadingShowable, loadingActivity: UIActivityIndicatorView, disposeBag: DisposeBag) {
        viewModel.onShowLoadingHud
            .bind(onNext: { (isLoading) in
                isLoading ?
                    loadingActivity.startAnimating() :
                    loadingActivity.stopAnimating()
            })
            .disposed(by: disposeBag)
    }
}
