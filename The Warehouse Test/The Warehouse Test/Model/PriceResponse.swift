//
//  PriceResponse.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct PriceResponse: Decodable {
    let prodQAT: String
    let found: Bool
    let product: Product?
    
    enum CodingKeys: String, CodingKey {
        case prodQAT
        case found
        case product
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        prodQAT = try container.decode(String.self, forKey: .prodQAT)
        
        if let foundString = try? container.decode(String.self, forKey: .found) {
            found = foundString == "Y"
        } else {
            found = false
        }
        
        product = try container.decode(Product.self, forKey: .product)
    }
}
