//
//  SearchResponse.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct SearchResponse: Decodable {
    let prodQAT: String
    let found: Bool
    let hitCount: Int?
    let results: [ResultModel]?
    
    enum CodingKeys: String, CodingKey {
        case prodQAT
        case found
        case hitCount
        case results
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        prodQAT = try container.decode(String.self, forKey: .prodQAT)
        
        if let foundString = try? container.decode(String.self, forKey: .found) {
            found = foundString == "Y"
        } else {
            found = false
        }
        
        if let hitCountString = try? container.decode(String.self, forKey: .hitCount) {
            hitCount = Int(hitCountString)
        } else {
            hitCount = nil
        }
        
        results = try container.decode([ResultModel].self, forKey: .results)
    }
}

struct ResultModel: Decodable {
    let description: String
    let products: [Product]
}
