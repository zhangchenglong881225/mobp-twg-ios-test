//
//  UserResponse.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct UserResponse: Decodable {
    let prodQAT: String
    let userID: String
}
