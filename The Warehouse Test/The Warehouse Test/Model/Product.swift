//
//  Product.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import Foundation

struct Product: Decodable {
    let barcode: String
    let description: String
    let imageURL: String
    let productKey: String
    
    let price: Price?
    let branchPrice: String?
}

struct Price: Decodable {
    let price: String
    let type: PriceType
    
    enum CodingKeys: String, CodingKey {
        case price
        case type
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        price = try container.decode(String.self, forKey: .price)
        
        if let typeString = try? container.decode(String.self, forKey: .type) {
            type = PriceType(rawValue: typeString) ?? .REG
        } else {
            type = .REG
        }
    }
}

enum PriceType: String {
    case REG
    case CLR
    case ADV
    case GRP
    case STO
    
    // MARK: - Readable String
    var readableString: String {
        switch self {
        case .REG:
            return ""
        case .CLR:
            return "Clearance"
        case .ADV:
            return "Discount"
        case .GRP:
            return "Group Discount"
        case .STO:
            return "Manager's special"
        }
    }
}
