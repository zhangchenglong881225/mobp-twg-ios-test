//
//  MainViewController.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import BarcodeScanner

extension MainViewController {
    static let toDetailSegueIdentifier = "toDetail"
}

class MainViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    private let sContrllr = UISearchController(searchResultsController: nil)
    private let barcodeController = BarcodeScannerViewController()
    
    private let disposeBag = DisposeBag()
    let viewModel = MainViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSearchController()
        setupBarcodeController()
        bindViewModel()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // Make search bar become first responder
        self.sContrllr.searchBar.becomeFirstResponder()
    }
    
    func bindViewModel() {
        viewModel.productCells.bind(to: self.tableView.rx.items) { [weak self] tableView, index, element in
            let indexPath = IndexPath(item: index, section: 0)
            guard let cell = self?.tableView.dequeueReusableCell(withIdentifier: "SubtitleCell", for: indexPath) else {
                return UITableViewCell()
            }
            cell.textLabel?.text = element.description
            cell.detailTextLabel?.text = element.barcode
            return cell
            }
            .disposed(by: disposeBag)
        
        self.tableView.rx.itemSelected
            .asObservable()
            .bind(onNext: { [weak self] indexPath in
                self?.tableView.deselectRow(at: indexPath, animated: true)
            })
            .disposed(by: disposeBag)
        
        self.tableView.rx.modelSelected(Product.self)
            .asObservable()
            .bind(onNext: { [weak self] product in
                self?.performSegue(withIdentifier: MainViewController.toDetailSegueIdentifier, sender: product.barcode)
            })
            .disposed(by: disposeBag)
        
        // Emit willDisplayCell event so that viewModel can monitor if last cells will display
        // and determine load more or not
        self.tableView.rx
            .willDisplayCell
            .asObservable()
            .map { $0.indexPath }
            .bind(to: self.viewModel.willDisplayCell)
            .disposed(by: disposeBag)
        
        self.sContrllr.searchBar.rx.searchButtonClicked
            .asObservable()
            .withLatestFrom(
                self.sContrllr.searchBar.rx.text
                    .orEmpty
                    .asObservable()
            )
            .subscribe(onNext: { [weak self] (term) in
                guard !term.isEmpty else {
                    return
                }
                self?.viewModel.search(withTerm: term)
            })
            .disposed(by: disposeBag)
        
        bindLoadingIndicator(viewModel: viewModel, loadingActivity: self.loadingActivity, disposeBag: disposeBag)
        
        bindErrorAlert(viewModel: viewModel, disposeBag: disposeBag)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case .some(let identifier):
            if identifier == MainViewController.toDetailSegueIdentifier {
                if let detailVC = segue.destination as? DetailViewController {
                    if let barcode = sender as? String {
                        detailVC.viewModel = DetailViewModel(barcode: barcode)
                    }
                }
            }
        default:
            break
        }
    }
    
    @IBAction func scanButtonPressed(_ sender: Any) {
        #if targetEnvironment(simulator)
        present(UIAlertController.getSingleAlert(withText: "Simulator not supported!"), animated: true, completion: nil)
        #else
        present(barcodeController, animated: true, completion: nil)
        #endif
    }
}

// MARK: - Search Controller
extension MainViewController {
    func setupSearchController() {
        // Setup the Search Controller
        sContrllr.obscuresBackgroundDuringPresentation = false
        sContrllr.searchBar.placeholder = "Search Products"
        navigationItem.searchController = sContrllr
        definesPresentationContext = true
    }
}

// MARK: - Barcode Controller
extension MainViewController: BarcodeScannerCodeDelegate, BarcodeScannerDismissalDelegate {
    
    func setupBarcodeController() {
        // Setup the Barcode Controller
        barcodeController.codeDelegate = self
        barcodeController.dismissalDelegate = self
        
        barcodeController.headerViewController.titleLabel.text = "Scan barcode"
        barcodeController.headerViewController.closeButton.setTitle("Close", for: .normal)
        
        barcodeController.isOneTimeSearch = false
    }
    
    func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
        controller.reset()
        controller.dismiss(animated: true) {
            self.performSegue(withIdentifier: MainViewController.toDetailSegueIdentifier, sender: code)
        }
    }
    
    func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
}
