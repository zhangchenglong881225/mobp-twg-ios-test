//
//  DetailViewController.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SDWebImage

class DetailViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var specialLabel: UILabel!
    @IBOutlet weak var descTextView: UITextView!
    @IBOutlet weak var loadingActivity: UIActivityIndicatorView!
    
    private let disposeBag = DisposeBag()
    var viewModel: DetailViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bindViewModel()
        viewModel?.getProductDetail()
    }
    
    func bindViewModel() {
        guard let viewModel = viewModel else { return }
        
        viewModel.productDetail
            .bind { [weak self] (product) in
                guard let `self` = self else {
                    return
                }
                if let urlString = product?.imageURL, let imageUrl = URL(string: urlString) {
                    self.imageView.sd_setImage(with: imageUrl, completed: nil)
                }
                self.priceLabel.text = product?.branchPrice?.nzCurrencyString()
                self.specialLabel.text = product?.price?.type.readableString
                self.descTextView.text = product?.description
            }
            .disposed(by: disposeBag)
        
        bindLoadingIndicator(viewModel: viewModel, loadingActivity: self.loadingActivity, disposeBag: disposeBag)
        
        bindErrorAlert(viewModel: viewModel, disposeBag: disposeBag)
    }
    
}
