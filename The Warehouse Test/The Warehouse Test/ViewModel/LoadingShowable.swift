//
//  LoadingShowable.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 30/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import RxSwift

protocol LoadingShowable {
    var onShowLoadingHud: Observable<Bool> { get }
}
