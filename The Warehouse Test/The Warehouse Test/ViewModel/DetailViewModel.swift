//
//  DetailViewModel.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import RxSwift
import RxCocoa

class DetailViewModel {
    private let disposeBag = DisposeBag()
    private let barcode: String
    
    var productDetail: Observable<Product?> {
        return product.asObservable()
    }
    
    private let product = BehaviorRelay<Product?>(value: nil)
    private let loadInProgress = BehaviorRelay<Bool>(value: false)
    private let errorMessage = BehaviorRelay<String>(value: "")
    
    init(barcode: String) {
        self.barcode = barcode
    }
}

// MARK: - Load product details
extension DetailViewModel {
    func getProductDetail() {
        self.loadInProgress.accept(true)
        APIClient.price(withBarcode: barcode) { (result) in
            switch result {
            case .success(let priceResponse):
                guard priceResponse.found else {
                    self.errorMessage.accept("Product not found.")
                    return
                }
                self.product.accept(priceResponse.product)
            case .failure(let error):
                self.errorMessage.accept("Error occurs!")
                print(error)
            }
            self.loadInProgress.accept(false)
        }
    }
}

// MARK: - ErrorShowable
extension DetailViewModel: ErrorShowable {
    var onShowError: Observable<String> {
        return errorMessage
            .asObservable()
            .distinctUntilChanged()
    }
}

// MARK: - LoadingShowable
extension DetailViewModel: LoadingShowable {
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
}
