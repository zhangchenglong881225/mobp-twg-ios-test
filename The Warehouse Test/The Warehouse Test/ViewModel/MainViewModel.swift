//
//  MainViewModel.swift
//  The Warehouse Test
//
//  Created by ZHANG Chenglong on 29/05/19.
//  Copyright © 2019 The Warehouse Group Limited. All rights reserved.
//

import RxSwift
import RxCocoa

class MainViewModel {
    private let disposeBag = DisposeBag()
    // Define which index will trigger load more action when count backwards
    private let loadMoreIndex = 4
    // Save search term for load more scenario
    private var searchTerm: String = ""
    private var searchResponse: SearchResponse?
    
    var productCells: Observable<[Product]> {
        return cells.asObservable()
    }
    let willDisplayCell = PublishSubject<IndexPath>()
    
    private let cells = BehaviorRelay<[Product]>(value: [])
    private let loadInProgress = BehaviorRelay<Bool>(value: false)
    private let errorMessage = BehaviorRelay<String>(value: "")
    
    init() {
        willDisplayCell
            .subscribe(onNext: { [weak self] indexPath in
                guard let `self` = self else {
                    return
                }
                if indexPath.row == self.cells.value.count - self.loadMoreIndex {
                    self.search()
                }
            })
            .disposed(by: disposeBag)
    }
}

// MARK: - Search products
extension MainViewModel {
    func search(withTerm termString: String = "") {
        // Return if load is in progress
        guard !loadInProgress.value else {
            return
        }
        // Return if search term is empty
        guard !termString.isEmpty || !searchTerm.isEmpty else {
            return
        }
        var term: String
        if termString.isEmpty {
            // Use last time search term if convenient func search() is called
            term = searchTerm
            // Return if all results are received
            guard self.cells.value.count < (self.searchResponse?.hitCount ?? Int.max) else {
                return
            }
        } else {
            // Save termString in searchTerm and clear cells value because search term changed
            term = termString
            searchTerm = termString
            self.cells.accept([])
        }
        self.loadInProgress.accept(true)
        // Search with start index (1-first index)
        APIClient.search(withTerm: term, start: self.cells.value.count + 1) { (result) in
            switch result {
            case .success(let searchResponse):
                self.searchResponse = searchResponse
                guard let results = searchResponse.results else {
                    self.cells.accept([])
                    return
                }
                let newProducts = results.flatMap{ $0.products }
                // Append new products onto existing ones
                self.cells.accept(self.cells.value + newProducts)
            case .failure(let error):
                self.errorMessage.accept("Error occurs!")
                print(error)
            }
            self.loadInProgress.accept(false)
        }
    }
}

// MARK: - ErrorShowable
extension MainViewModel: ErrorShowable {
    var onShowError: Observable<String> {
        return errorMessage
            .asObservable()
            .distinctUntilChanged()
    }
}

// MARK: - LoadingShowable
extension MainViewModel: LoadingShowable {
    var onShowLoadingHud: Observable<Bool> {
        return loadInProgress
            .asObservable()
            .distinctUntilChanged()
    }
}
