//
//  ViewController.h
//  The Warehouse Test
//
//  Created by John Douglas on 10/10/18.
//  Copyright © 2018 The Warehouse Group Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)scanBarcodeButtonPressed:(id)s;
@property (strong, nonatomic) IBOutlet UIButton *sBB;
@property (strong, nonatomic) UISearchController *sContrllr;

@end

